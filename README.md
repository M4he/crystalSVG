# Crystal SVG Remaster icon set

An icon theme remaster of **Crystal SVG**, originally designed by Everaldo Coelho and used throughout the KDE 3 era.

The icon theme has been updated to support modern environments such as Gnome and Xfce.
Furthermore, some broken or mismatching icons have been fixed and some additional icons added.

## Credits

- based on [MartinF99's KDE5 port](https://github.com/MartinF99/crystalSVG)
- some additional icons taken from [Wikimedia Commons](https://commons.wikimedia.org/wiki/Category:Crystal_128)
- OpenOffice.org 3.2.1 icons retrieved from its [source package](https://archive.apache.org/dist/incubator/ooo/stable/3.2.1/OOo_3.2.1_src_core.tar.bz2)
- `xfce4-notes-plugin` icon (also used for `xpad`) taken from [elementary-xfce](https://github.com/shimmerproject/elementary-xfce) icon set

All icons originate in the Crystal Project, originally designed by [Everaldo Coelho](https://www.everaldo.com/about).


# Installation

Copy the whole folder of the repository to either of the following locations:
- `/usr/share/icons` for usage of all users (requires root permissions)
- OR `~/.local/share/icons` for a single user on KDE systems
- OR `~/.icons` for single user on GTK systems like Mate, Gnome, Cinnamon, Xfce 


# Toolkit

This repository contains some tools to help expand and maintain the icon set.

## Dupe icons (symlink)

If you want to use an existing icon for a new application.

A) the icon is in the same category (e.g. 'apps'):
```
# ./dupe_icon.py <existing-icon-name> <new-icon-name>
./dupe_icon.py konqueror firefox
```
... will create an `apps/firefox.{png,svgz,svg}` symbolic link that points to `konqueror.{png,svgz,svg}` in each size directory

B) the existing icon is in a different category (e.g. 'devices') than the new one you want to create (e.g. 'apps'):
```
# ./dupe_icon.py <existing-icon-name> <new-icon-category> <new-icon-name>
./dupe_icon.py pda_blue apps handheld-manager
```
... will create an `apps/handeld-manager.{png,svgz,svg}` symbolic link that points to `../devices/pda_blue.{png,svgz,svg}` (relative path) within each size directory.

**NOTE:** always use `gtk-update-icon-cache <path-to-this-directory>` afterwards.

**NOTE:** use `copy_icon.py` instead of `dupe_icon.py` to create a genuine copy instead of a symlink.

## Render SVG icons

If there is an `.svgz` or `.svg` icon within `scalable/*/` that is not yet rendered as `.png` in the other size directories:

```
# ./render_icon.py <svg-icon-name> <target-category> <target-icon-name>
./render_icon.py display devices display
```
... will render a `devices/display.png` to each size directory based on `scalable/*/display.svgz` or `scalable/*/display.svg` respectively.

**NOTE:** always use `gtk-update-icon-cache <path-to-this-directory>` afterwards.
