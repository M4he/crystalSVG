#!/usr/bin/env python3
import os
import sys
import subprocess

INKSCAPE_PATH="/opt/Inkscape-3bc2e81-x86_64.AppImage"

# args: <source_name> <target_category> <target_name>

if len(sys.argv) < 4:
    print('wrong number of arguments')

existing_svgz = sys.argv[1]
render_category = sys.argv[2]
render_name = sys.argv[3]

root = '.'

def find_source(name):
    _root = os.path.join(root, 'scalable')
    full_name = name + '.svg'
    for type_dir_name in os.listdir(_root):
        type_dir_path = os.path.join(_root, type_dir_name)

        if not os.path.isdir(type_dir_path):
            continue

        if full_name in os.listdir(type_dir_path):
            return os.path.join(type_dir_path, full_name)

        # retry with svgz format
        if full_name + 'z' in os.listdir(type_dir_path):
            return os.path.join(type_dir_path, full_name + 'z')

    raise Exception('Source icon not found!')


def render_icon(source_path, target_path, size):
    cmd = [
        INKSCAPE_PATH,
        '--export-type=png',
        '--export-filename=%s' % target_path,
        '--export-width=%s' % str(size),
        '--export-height=%s' % str(size),
        source_path
    ]
    subprocess.run(cmd)


source = find_source(existing_svgz)

for size_dir_name in os.listdir(root):
    # ignore non-size directories that don't adhere to <width>x<height> naming
    if 'x' not in size_dir_name:
        continue
    size_val = size_dir_name[:size_dir_name.find('x')]  # extract size value
    size_dir_path = os.path.join(root, size_dir_name)

    # filter non-directories
    if not os.path.isdir(size_dir_path):
        continue

    # check if the target category dir exists within that size dir
    if render_category not in os.listdir(size_dir_path):
        print('WARN: category %s not found in %s, ignoring...' % (
            render_category, size_dir_name))
        continue

    target = os.path.join(size_dir_path, render_category, render_name + '.png')

    if os.path.exists(target):
        print('WARN: %s already exists, skipping...' % target)
        continue

    print("%s -> %s @ %s" % (source, target, size_val))
    render_icon(source, target, size_val)
